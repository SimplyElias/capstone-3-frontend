import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import NavBar from '../components/NavBar';
import { UserProvider } from '../UserContext';
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "@fortawesome/fontawesome-svg-core/styles.css"; // import Font Awesome CSS
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false; // Tell Font Awesome to skip adding the CSS automatically since it's being imported above

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
	// Initialized as an object with properties from localStorage
	id: null,
	isAdmin: null,
	firstName: null
	})

	const unsetUser = () => {
	localStorage.clear();

	setUser({
		id: null,
		isAdmin: null,
		firstName: null
		})
	}

	useEffect(() => {

		setUser({
			id: localStorage.getItem('id'),
			isAdmin: localStorage.getItem('isAdmin'),
			firstName: localStorage.getItem('firstName')
		})

	}, [])

    return (
  		<React.Fragment>
  			<UserProvider value={{ user, setUser, unsetUser }}>
  			<NavBar />
  			<Component {...pageProps} />
  			</UserProvider>
  		</React.Fragment>
  		)
}

export default MyApp

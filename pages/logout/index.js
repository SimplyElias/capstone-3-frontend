import React, { useContext, useEffect } from 'react';
import Router from 'next/router'
import UserContext from '../../UserContext';


export default function index(){

	const { unsetUser } = useContext(UserContext);


	useEffect(() => {
		/// From app.js
	unsetUser();
	Router.push('/');
	}, [])
	

	return null

}
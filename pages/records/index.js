import React, { useState, useEffect, useContext } from 'react'
import Router from 'next/router'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../../app-helper'
import { Container, CardColumns, Row, Col, Card, Button, Alert, Dropdown, Form, Table } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import Record from '../../components/Record';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faSearch, faPlus } from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function index() {
	const router = useRouter()
	const {user, setUser} = useContext(UserContext)
	const [show, setShow] = useState(false);

	const handleClick = (e) => {
    e.preventDefault()
    router.push("/login")
 	}

		const [records, setRecords] = useState([]);
		const [recordSearch, setrecordSearch] = useState('');
		const [recordType, setrecordType] = useState('All');
		const [recordCards, setrecordCards] = useState([]);
		const [recordsLength, setRecordsLength] = useState(0);
		const [incomeRecords, setIncomeRecords] = useState(0);
		const [expenseRecords, setExpenseRecords] = useState(0)

		const handleSelect=(e)=>{
    		console.log(e);
    		setrecordType(e)
 		}

		useEffect(() => {
			fetch(`${AppHelper.API_URL}/users/records`, { 
				method: "POST",
				headers: 
				{
				 Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify(
				{
					recordSearchType: recordType
				})
			}) // From the backend.
			.then(AppHelper.toJSON)
			.then(data => {
				console.log(data)
				setRecords(data)
				setRecordsLength(0)
				if (data.error !== "no-records") {
					setRecordsLength(data.length)
					setrecordCards(data.map((record) => {
					return <Record key={record._id} record={record} />
				})
				)
				}
				// setreRun(true)
				console.log(records)
			})

			
		}, [])

		useEffect(() => {
			let updatedRecords = records.map((record) => {
				if((record.recordType === recordType) || (recordType === "All")) {
					if (recordSearch !== null) {
							if(record.recordDescription.includes(recordSearch)) {
								return <Record key={record._id} record={record} />
							}
					}

					else {
						return <Record key={record._id} record={record} />
					}
					
				}
				else {
					return null
				}
			})
			setrecordCards(updatedRecords)

		}, [recordSearch, recordType])
		

		function filter(e) {
			e.preventDefault();

			const options = {
				method: "POST",
				headers: 
				{
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify(
				{
					recordSearchType: recordType
				})
			}
			fetch(`${ AppHelper.API_URL}/users/records`, options)
			.then(AppHelper.toJSON)
			.then(data => {
				setRecords(data)
			})
		}


	return(
		<React.Fragment>
		<Head>
			<title> My Records | Budgeteer </title>
		</Head>
		{
		(user.id === null) ? 
			<Container>
				<Alert show={show} variant="danger">
			        <Alert.Heading>An error occurred!</Alert.Heading>
			        <p>
			          You are not logged in to access this page!
			        </p>
			        <hr />
			        <div className="d-flex justify-content-end">
			          <Button onClick={handleClick} variant="outline-success">
			            Login
			          </Button>
			        </div>
			     </Alert>
			</Container>
			:
			<Container className="mt-5 pt-5 mb-5">
				<h1> Records </h1>
				<p> You currently have {recordsLength} records in your account! </p>
				<Row>
				<Col>
					<Button variant="success" type="submit" id="addBtn" onClick={() => setShow(true)}>
					<FontAwesomeIcon icon={faPlus} /> Add New Record</Button>
				    <AddNewRecord
				    	show={show}
				    	onHide={() => setShow(false)}
				    />
				</Col>
				</Row>
				<Form onSubmit={(e) => filter(e)}>
					<Row>
						<Col xs md="4">
					        <Form.Group controlId="recordSearch">
						        <Form.Label>Record Search: </Form.Label>
						        <Form.Control
						        	type="text"
						        	placeholder="Enter Record Description"
						        	onChange={(e) => setrecordSearch(e.target.value)}
						        />
						    </Form.Group>
						</Col>
						<Col xs md="4">
						    <Form.Group controlId="recordType">
								<Form.Label>Record Type: </Form.Label>
							    	<Dropdown onSelect={handleSelect}> 
										<Dropdown.Toggle variant="primary" id="dropdown">
											{recordType}
										</Dropdown.Toggle>
										<Dropdown.Menu>
								    		<Dropdown.Item eventKey="All">All</Dropdown.Item>
								    		<Dropdown.Item eventKey="Income">Income</Dropdown.Item>
								    		<Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
								  		</Dropdown.Menu>
									</Dropdown>
					        </Form.Group>
			   			</Col>   			
					</Row>
				</Form>
				<Col xs md="12">
					{(records.length > 0) ? 
						<React.Fragment>
						<Table striped bordered hover responsive size="sm">
						<thead>
							<tr>
								<th>Record Date (UTC)</th>
								<th>Record Description</th>
								<th>Record Info</th>
								<th>Amount (In PHP)</th>
								<th className="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						{recordCards}
						</tbody>
						</Table>
						</React.Fragment>
					:
						<Alert variant="info">
			            	You currently have no records!
			            </Alert> 
					}
				</Col>
			</Container>
		}

		</React.Fragment>
		)
}

function AddNewRecord(props) {

	const router = useRouter()
	const [dropdownCategory, setDropdownCategory] = useState('Select Category')
	const [recordDescription, setrecordDescription] = useState('')
	const [newrecordType, setnewrecordType] = useState('Income')
	const [recordCategory, setrecordCategory] = useState('')
	const [recordCategories, setRecordCategories] = useState([])
	const [recordAmount, setrecordAmount] = useState(0)
	console.log(typeof recordAmount)

	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/categories`, { 
			headers: 
			{
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)

			setRecordCategories(data.map((category) => {
				if (category.categoryType === newrecordType) {
					return <Dropdown.Item key={category._id} eventKey={category.categoryName}> {category.categoryName}</Dropdown.Item>
				}	
			})
			)

		})
	}, [newrecordType])

	function addNew(e) {
		e.preventDefault();
		const options = { 
			method: "POST",
			headers: 
			{
			 'Content-Type': 'application/json',
			 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			{
				recordType: newrecordType,
				recordCategory: recordCategory,
				recordAmount: recordAmount,
				recordDescription: recordDescription
			})
		}
		fetch(`${AppHelper.API_URL}/users/records/new`, options) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					icon: 'success',
					title: `You have successfully created a new ${newrecordType} record!`,
				}).then(() => {
					router.reload('/records')
				})
			}
			else if(data.error === "no-changes") {
				Swal.fire({
					icon: 'info',
					title: `You made no changes to the record with ID: ${recordId}!`,
				}).then(() => {
					router.reload('/records')
				})
			}
		})
	}
	
	const handleSelect=(e)=>{
		setnewrecordType(e)
		}

		const handleCategorySelect=(e)=>{
			setDropdownCategory(e)
		setrecordCategory(e)
		}

  return (
  	<React.Fragment>
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered

    >
    <Form onSubmit={(e) => addNew(e)}>
    <Modal.Header closeButton>
    	<Modal.Title id="contained-modal-title-vcenter">
        	Add New Record
        </Modal.Title>
    </Modal.Header>
    <Modal.Body>
        	<Col xs md="6">
			    <Form.Group controlId="recordDescription">
				    <Form.Label>Record Description: </Form.Label>
				    <Form.Control
				        type="text"
				        placeholder="Enter Transaction Description"
				        onChange={(e) => setrecordDescription(e.target.value)}
				    />
				</Form.Group>
			</Col>
			<Col xs md="6">
				<Form.Group controlId="recordType">
					<Form.Label>Record Type: </Form.Label>
					<Dropdown onSelect={handleSelect}> 
						<Dropdown.Toggle variant="success" id="dropdown">
							{newrecordType}
						</Dropdown.Toggle>
						<Dropdown.Menu>
						    <Dropdown.Item eventKey="Income">Income</Dropdown.Item>
						    <Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
			    </Form.Group>
	   		</Col>
			<Col xs md="6">
				<Form.Group controlId="recordCategory">
					<Form.Label>Record Category: </Form.Label>
					<Dropdown onSelect={handleCategorySelect}> 
						<Dropdown.Toggle variant="success" id="dropdown">
							{dropdownCategory}
						</Dropdown.Toggle>
						<Dropdown.Menu>
						    {(recordCategories.length > 0) ?
								recordCategories
							:
								null
							}
						</Dropdown.Menu>
					</Dropdown>
			    </Form.Group>
	   		</Col>
	   		<Col xs md="6">
				<Form.Group controlId="recordAmount">
					<Form.Label>Record Amount: </Form.Label>
				    <Form.Control
				        type="number"
				        placeholder="0"
				        onChange={(e) => setrecordAmount(parseInt(e.target.value))}
				    />
			    </Form.Group>
	   		</Col>
    </Modal.Body>
    <Modal.Footer>
    	<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
    </Modal.Footer>
    </Form>
    </Modal>
    </React.Fragment>
  );
}
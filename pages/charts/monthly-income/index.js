import React, { useState, useEffect, useContext } from 'react'
import Head from 'next/head'
import { Form, Button, Row, Col, Alert, Container} from 'react-bootstrap'
import MonthlyIncome from '../../../components/MonthlyIncome'
import AppHelper from '../../../app-helper'
import Swal from 'sweetalert2'
import UserContext from '../../../UserContext';
import moment from 'moment'

export default function index() {

    const { user } = useContext(UserContext);
    const [show, setShow] = useState(true);
    const handleClick = (e) => {
    e.preventDefault()
    router.push("/login")
    }

    const [incomeData, setincomeData] = useState([])
    const [incomeLength, setincomeLength] = useState(0)
    

    // This function converts a file into base64.
    // The base64 format is basically a string representation of the converted file.

    useEffect(() => {

        fetch(`${AppHelper.API_URL}/users/records`, { 
                method: "POST",
                headers: 
                {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(
                {
                    recordSearchType: 'Income'
                })
            }) // From the backend.
            .then(AppHelper.toJSON)
            .then(data => {
                // console.log(data)
                // console.log(moment(data.recordedOn).format('yyyy-MM-DD'))
                console.log(data)
                setincomeData(data)
                setincomeLength(data.length)

            })
    }, [])

    return (
        <React.Fragment>
        <Head>
            <title>Monthly Income | Budgeteer </title>
        </Head>
        {(user !== null) ?
            <Container className="mt-5 pt-5 mb-5">
                <h1> Monthly Income for 2021 (in PHP) </h1>
                <Row>
                    <Col md={12}>
                        <MonthlyIncome incomeData={incomeData} />
                    </Col>
                </Row>   
            </Container>    
        :
        <Container className="mt-5 pt-5 mb-5"> >
            <Alert show={show} variant="danger">
                <Alert.Heading>An error occurred!</Alert.Heading>
                <p>
                  You are not logged in to access this page!
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                  <Button onClick={handleClick} variant="outline-success">
                    Login
                  </Button>
                </div>
            </Alert>
        </Container>
        }
        </React.Fragment> 
    )
}
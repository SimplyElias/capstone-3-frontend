import React, { useState, useEffect, useContext } from 'react'
import Head from 'next/head'
import { Form, Button, Row, Col, Alert, Container} from 'react-bootstrap'
import MonthlyExpense from '../../../components/MonthlyExpense'
import AppHelper from '../../../app-helper'
import Swal from 'sweetalert2'
import UserContext from '../../../UserContext';
import moment from 'moment'
import { useRouter } from 'next/router'

export default function index() {
    
    const router = useRouter()
    const { user } = useContext(UserContext);
    const [show, setShow] = useState(true);
    const handleClick = (e) => {
    e.preventDefault()
    router.reload("/records")
    }

    const [expenseData, setexpenseData] = useState([])
    const [expenseLength, setexpenseLength] = useState(0)
    

    // This function converts a file into base64.
    // The base64 format is basically a string representation of the converted file.

    useEffect(() => {

        fetch(`${AppHelper.API_URL}/users/records`, { 
                method: "POST",
                headers: 
                {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(
                {
                    recordSearchType: 'Expense'
                })
            }) // From the backend.
            .then(AppHelper.toJSON)
            .then(data => {
                // console.log(data)
                // console.log(moment(data.recordedOn).format('yyyy-MM-DD'))
                console.log(data)
                setexpenseData(data)
                setexpenseLength(data.length)

            })
    }, [])

    return (
        <React.Fragment>
        <Head>
            <title>Monthly Expense | Budgeteer </title>
        </Head>
        {(user !== null) ?
            <Container className="mt-5 pt-5 mb-5">
                <h1> Monthly Expense for 2021 (in PHP) </h1>
                <Row>
                    <Col md={12}>
                        <MonthlyExpense expenseData={expenseData} />
                    </Col>
                </Row>   
            </Container>    
        :
        <Container className="mt-5 pt-5 mb-5"> >
            <Alert show={show} variant="danger">
                <Alert.Heading>An error occurred!</Alert.Heading>
                <p>
                  You are not logged in to access this page!
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                  <Button onClick={handleClick} variant="outline-success">
                    Login
                  </Button>
                </div>
            </Alert>
        </Container>
        }
        </React.Fragment> 
    )
}
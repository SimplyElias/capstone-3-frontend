import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { Form, Button, Row, Col, Alert, Dropdown, Container } from 'react-bootstrap'
import AppHelper from '../../../app-helper'
import Swal from 'sweetalert2'
import UserContext from '../../../UserContext';
import { Line } from 'react-chartjs-2';
import moment from 'moment'
// import BudgetTrend from '../../../components/BudgetTrend';

export default function index() {
    const [recordData, setrecordData] = useState([])
    const [recordFiltered, setrecordFiltered] = useState([])
    const [startDate, setstartDate] = useState('')
    const [endDate, setendDate] = useState('')
    const [recordType, setrecordType] = useState('All')
    const [dateData, setDateData] = useState([])
    const [amountData, setAmountData] = useState([])
    const [incomeData, setIncomeData] = useState([])
    const [expenseData, setExpenseData] = useState([])
    const [days, setDays] = useState(0)
    const [recordLength, setRecordLength] = useState(0)

    useEffect(() => {
			fetch(`${AppHelper.API_URL}/users/records`, { 
				method: "POST",
				headers: 
				{
				 Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify(
				{
					recordSearchType: 'All'
				})
			}) // From the backend.
			.then(AppHelper.toJSON)
			.then(data => {

                if(data.length > 0) {
                    setrecordData(data)
                    setRecordLength(data.length)
                }     

			})

		}, [])

    useEffect(() => {

        fetch(`${AppHelper.API_URL}/users/records`, { 
                method: "POST",
                headers: 
                {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(
                {
                    recordSearchType: 'All'
                })
            }) // From the backend.
            .then(AppHelper.toJSON)
            .then(data => {
                // console.log(data)
                // console.log(moment(data.recordedOn).format('yyyy-MM-DD'))

                if (startDate !== '' && endDate !== '') {
                    let filteredTransactions = []

                    data.forEach((record) => {
                        if(moment(record.recordedOn).format('yyyy-MM-DD') >= moment(startDate).format('yyyy-MM-DD') && moment(record.recordedOn).format('yyyy-MM-DD') <= moment(endDate).format('yyyy-MM-DD')) {

                            filteredTransactions.push(record)
                        }
                    })

                    setrecordFiltered(filteredTransactions)
                    setRecordLength(filteredTransactions.length)
                }
                // console.log(filteredTransactions)

                // data.forEach(entry => {
                //     if(date.indexOf(moment(entry.recordedOn).format('MMMM DD YY')) === -1 &&)
                // })
                // console.log(startDate)
                // console.log(endDate)
                

            })
    }, [startDate, endDate])

    useEffect(() => {

        let dateArray = []
        let incomeArray = []
        let expenseArray = []
        let budgetArray = []

        recordData.forEach((record) => {
            dateArray.push(moment(record.recordedOn).format('yyyy-MM-DD'))
            budgetArray.push(record.balanceAfterRecord)
            if(record.recordType === "Income") {
                incomeArray.push(record.recordAmount)
            }
            else if(record.recordType === "Expense") {
                expenseArray.push(record.recordAmount)
            }
        })

        setDateData(dateArray)
        setAmountData(budgetArray)
        setIncomeData(incomeArray)
        setExpenseData(expenseArray)

    }, [recordData])


    useEffect(() => {

        let dateArray = []
        let incomeArray = []
        let expenseArray = []
        let budgetArray = []

        recordFiltered.forEach((record) => {
            dateArray.push(moment(record.recordedOn).format('yyyy-MM-DD'))
            budgetArray.push(record.balanceAfterRecord)
            if(record.recordType === "Income") {
                incomeArray.push(record.recordAmount)
            }
            else if(record.recordType === "Expense") {
                expenseArray.push(record.recordAmount)
            }
        })

        setDateData(dateArray)
        setAmountData(budgetArray)
        setIncomeData(incomeArray)
        setExpenseData(expenseArray)

    }, [recordFiltered])

    const dataBudget =  {
        labels: dateData,
        datasets: [
        { 
            label: 'Budget Trend',
            data: amountData,
            backgroundColor: 'rgba(28, 28, 231, 0.6)',
        }
        ],

    }
    const dataIncome =  {
        labels: dateData,
        datasets: [
        { 
            label: 'Income',
            data: incomeData,
            backgroundColor: 'rgba(28, 231, 28, 0.6)',
        }
        ],
    }
    const dataExpense =  {
        labels: dateData,
        datasets: [
        { 
            label: 'Expenses',
            data: expenseData,
            backgroundColor: 'rgba(241, 8, 8, 0.86)',
        }
        ],
    }

    const option = {
      tooltips: {
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var meta = dataset._meta[Object.keys(dataset._meta)[0]];
            var total = meta.total;
            var currentValue = dataset.data[tooltipItem.index];
            var percentage = parseFloat((currentValue/total*100).toFixed(1));
            return currentValue + ' (' + percentage + '%)';
          },
          title: function(tooltipItem, data) {
            return data.labels[tooltipItem[0].index];
          }
        }
      }
    }

        return (
        <React.Fragment>
            <Head>
                <title>Budget Trend</title>
            </Head>
            <Container className="mt-5 pt-5 mb-5">
            <h1> Budget Trend Data </h1>
            <p> View your balance after each record, as well as your Income and Expenses Data here! </p>
            <p> Please set a start and end date for filtering! </p>
            <p> You have <b>{recordLength}</b> record/s on this date range. </p>
            <Form>
            <Row>
                <Col md="6">
                <Form.Label> Start Date: </Form.Label>
                <Form.Control type="date" onChange={(e) => setstartDate(e.target.value)}/>
                </Col>
                <Col md="6">
                <Form.Label> End Date: </Form.Label>
                <Form.Control type="date" onChange={(e) => setendDate(e.target.value)}/>
                </Col>
            </Row>
            </Form>
            </Container>
            <Container className="mt-5 pt-5 mb-5">
            { recordData.length > 0 
                ?
                <Row>
                    <Col md={12}>
                        <Line data={dataBudget} options={option} redraw={true}/>
                    </Col>
                    <Col md={6}>
                        <Line data={dataIncome} redraw={true}/>
                    </Col>
                    <Col md={6}>
                        <Line data={dataExpense} redraw={true}/>
                    </Col>
                </Row>
                :
                    <Alert variant="info">
                        No data found.
                    </Alert>       
            }
            </Container>
        </React.Fragment>
    )
}
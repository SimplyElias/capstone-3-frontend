import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { Form, Button, Row, Col, Alert, Container } from 'react-bootstrap'
import AppHelper from '../../../app-helper'
import Swal from 'sweetalert2'
import UserContext from '../../../UserContext';
import { Pie } from 'react-chartjs-2';
import randomColor from 'randomcolor';
import moment from 'moment'

export default function index() {

    const [recordType, setrecordType] = useState('All')
    const [recordData, setrecordData] = useState([])
    const [recordFiltered, setrecordFiltered] = useState([])
    const [startDate, setstartDate] = useState('')
    const [endDate, setendDate] = useState('')
    const [dateData, setDateData] = useState([])
    const [categoriesAmount, setCategoriesAmount] = useState([])
    const [categories, setCategories] = useState([])
    const [incomeData, setIncomeData] = useState([])
    const [incomeCategories, setIncomeCategories] = useState([])
    const [expenseData, setExpenseData] = useState([])
    const [expenseCategories, setExpenseCategories] = useState([])
    const [days, setDays] = useState(0)
    const [recordLength, setRecordLength] = useState(0)
    const [bgColors, setBgColors] = useState([]);
    const [incomeBgColors, setIncomeBgColors] = useState([]);
    const [expenseBgColors, setExpenseBgColors] = useState([]);


    useEffect(() => {
        fetch(`${AppHelper.API_URL}/users/records`, { 
            method: "POST",
            headers: 
            {
             Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(
            {
                recordSearchType: 'All'
            })
        }) // From the backend.
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data)

            if(data.length > 0) {
                setrecordData(data)
                setRecordLength(data.length)
            }  

        })

    }, [])
    
    useEffect(() => {

        fetch(`${AppHelper.API_URL}/users/records`, { 
                method: "POST",
                headers: 
                {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify(
                {
                    recordSearchType: 'All'
                })
            }) // From the backend.
            .then(AppHelper.toJSON)
            .then(data => {
                // console.log(data)
                // console.log(moment(data.recordedOn).format('yyyy-MM-DD'))

                if (startDate !== '' && endDate !== '') {
                    let filteredTransactions = []

                    data.forEach((record) => {
                        if(moment(record.recordedOn).format('yyyy-MM-DD') >= moment(startDate).format('yyyy-MM-DD') && moment(record.recordedOn).format('yyyy-MM-DD') <= moment(endDate).format('yyyy-MM-DD')) {

                            filteredTransactions.push(record)
                        }
                    })

                    setrecordFiltered(filteredTransactions)
                    setRecordLength(filteredTransactions.length)
                }
                // console.log(filteredTransactions)

                // data.forEach(entry => {
                //     if(date.indexOf(moment(entry.recordedOn).format('MMMM DD YY')) === -1 &&)
                // })
                // console.log(startDate)
                // console.log(endDate)
                

            })
    }, [startDate, endDate])

    useEffect(() => {

        let incomeCategory = []
        let expenseCategory = []
        let storedCategories = []

        recordData.forEach((entry) => {
            if (storedCategories.indexOf(entry.recordCategory) === -1) {
                storedCategories.push(entry.recordCategory)
                if (incomeCategory.indexOf(entry.recordCategory) === -1) {
                    if (entry.recordType === "Income") {
                        incomeCategory.push(entry.recordCategory)
                    }
                }
                if (expenseCategory.indexOf(entry.recordCategory) === -1) {
                    if (entry.recordType === "Expense") {
                        expenseCategory.push(entry.recordCategory)
                    }
                }
            }
            
        })
        setCategories(storedCategories)
        setIncomeCategories(incomeCategory)
        setExpenseCategories(expenseCategory)

        // For all categories
        setCategoriesAmount(storedCategories.map((category) => {
            let amount = 0
            recordData.forEach((entry) => {
                if (entry.recordCategory === category) {
                    amount += parseInt(entry.recordAmount)
                }
            })
            return amount
        })
        )

        // For Income Categories
        setIncomeData(incomeCategory.map((category) => {
            let amount = 0
            recordData.forEach((entry) => {
                if (entry.recordCategory === category) {
                    amount += parseInt(entry.recordAmount)
                }
            })
            return amount
        })
        )

        setExpenseData(expenseCategory.map((category) => {
            let amount = 0
            recordData.forEach((entry) => {
                if (entry.recordCategory === category) {
                    amount += parseInt(entry.recordAmount)
                }
            })
            return amount
        })
        )

        setBgColors(storedCategories.map(() => randomColor({
            hue: 'blue',
            luminosity: 'dark',
            format: 'rgba',
            alpha: 0.5
            })
        ))
        setIncomeBgColors(incomeCategory.map(() => randomColor({
            hue: 'green',
            luminosity: 'dark',
            format: 'rgba',
            alpha: 0.5
            })
        ))
        setExpenseBgColors(expenseCategory.map(() => randomColor({
            hue: 'red',
            luminosity: 'dark',
            format: 'rgba',
            alpha: 0.5
            })
        ))

    }, [recordData])


    useEffect(() => {

        let incomeCategory = []
        let expenseArray = []
        let expenseCategory = []
        let storedCategories = []

        recordFiltered.forEach((entry) => {
            if (storedCategories.indexOf(entry.recordCategory) === -1) {
                storedCategories.push(entry.recordCategory)
            }
            if (incomeCategory.indexOf(entry.recordCategory) === -1 && entry.recordType === "Income") {
                incomeCategory.push(entry.recordCategory)
            }
            if (expenseCategory.indexOf(entry.recordCategory) === -1 && entry.recordType === "Expense") {
                expenseCategory.push(entry.recordCategory)
            }
        })

        setCategories(storedCategories)
        setIncomeCategories(incomeCategory)
        setExpenseCategories(expenseCategory)
        // For all categories
        setCategoriesAmount(storedCategories.map((category) => {
            let amount = 0
            recordFiltered.forEach((entry) => {
                if (entry.recordCategory === category) {
                    amount += parseInt(entry.recordAmount)
                }
            })
            return amount
        })
        )

        // For Income Categories
        setIncomeData(incomeCategories.map((category) => {
            let amount = 0
            recordFiltered.forEach((entry) => {
                if (entry.recordCategory === category) {
                    amount += parseInt(entry.recordAmount)
                }
            })
            return amount
        })
        )

        setExpenseData(expenseCategories.map((category) => {
            let amount = 0
            recordFiltered.forEach((entry) => {
                if (entry.recordCategory === category) {
                    amount += parseInt(entry.recordAmount)
                }
            })
            return amount
        })
        )

        setBgColors(storedCategories.map(() => randomColor({
            hue: 'blue',
            luminosity: 'dark',
            format: 'rgba',
            alpha: 0.5
            })
        ))
        setIncomeBgColors(incomeCategory.map(() => randomColor({
            hue: 'green',
            luminosity: 'dark',
            format: 'rgba',
            alpha: 0.5
            })
        ))
        setExpenseBgColors(expenseCategory.map(() => randomColor({
            hue: 'red',
            luminosity: 'dark',
            format: 'rgba',
            alpha: 0.5
            })
        ))


    }, [recordFiltered])



    const dataAll =  {
        labels: categories,
        datasets: [
        { 
            data: categoriesAmount,
            backgroundColor: bgColors,
            hoverBackgroundColor: bgColors
        }
        ],

    }
    const dataIncome =  {
        labels: incomeCategories,
        datasets: [
        { 
            data: incomeData,
            backgroundColor: incomeBgColors,
            hoverBackgroundColor: incomeBgColors
        }
        ],

    }
    const dataExpense =  {
        labels: expenseCategories,
        datasets: [
        { 
            data: expenseData,
            backgroundColor: expenseBgColors,
            hoverBackgroundColor: expenseBgColors
        }
        ],

    }
    const option = {
      tooltips: {
        callbacks: {
          label: function(tooltipItem, data) {
            var dataset = data.datasets[tooltipItem.datasetIndex];
            var meta = dataset._meta[Object.keys(dataset._meta)[0]];
            var total = meta.total;
            var currentValue = dataset.data[tooltipItem.index];
            var percentage = parseFloat((currentValue/total*100).toFixed(1));
            return currentValue + ' (' + percentage + '%)';
          },
          title: function(tooltipItem, data) {
            return data.labels[tooltipItem[0].index];
          }
        }
      }
    }

        return (
        <React.Fragment>
            <Head>
                <title>Category Breakdown</title>
            </Head>
            <Container className="mt-5 pt-5 mb-5">
            <h1> Breakdown</h1>
            <p> View your account data throughout all your categories, as well as your Income and Expenses Data per category here! </p>
            <p> Please set a start and end date for filtering! </p>
            <p> You have <b>{recordLength}</b> record/s on this date range. </p>
            <Form>
            <Row>
                <Col md="6">
                <Form.Label> Start Date: </Form.Label>
                <Form.Control type="date" onChange={(e) => setstartDate(e.target.value)}/>
                </Col>
                <Col md="6">
                <Form.Label> End Date: </Form.Label>
                <Form.Control type="date" onChange={(e) => setendDate(e.target.value)}/>
                </Col>
            </Row>
            </Form>
            </Container>
            <Container className="mt-5 pt-5 mb-5">
            { recordData.length > 0 
                ?
                <Row>
                    <Col md={12}>
                        <h5 className="text-center text-primary"> All Categories </h5>
                        <Pie data={dataAll} options={option} redraw={true}/>
                    </Col>
                    <Col md={6}>
                        <h5 className="text-center text-success"> Income Categories </h5>
                        <Pie data={dataIncome} options={option} redraw={true}/>
                    </Col>
                    <Col md={6}>
                        <h5 className="text-center text-danger"> Expenses Categories </h5>
                        <Pie data={dataExpense} options={option} redraw={true}/>
                    </Col>
                </Row>
                :
                    <Alert variant="info">
                        No data found.
                    </Alert>       
            }
            </Container>
        </React.Fragment>
    )
}
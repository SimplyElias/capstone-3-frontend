import React, { useState, useEffect, useContext } from 'react'
import Router from 'next/router'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../../app-helper'
import { Container, Row, Col, Card, Button, Alert, Dropdown, Form, Table } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import Category from '../../components/Category';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faPlus } from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function index() {
	const router = useRouter()
	const {user, setUser} = useContext(UserContext)
	const [show, setShow] = useState(true);
	const [modalShow, setModalShow] = React.useState(false);
	const [categoryCards, setCategoryCards] = useState([])

	const [categorySearch, setcategorySearch] = useState('')
	const [categorysearchType, setcategorysearchType] = useState('All')

	const handleSelect=(e)=>{
    	setcategorysearchType(e)
 	}
 	
	const handleClick = (e) => {
    e.preventDefault()
    router.push("/login")
 	}
	console.log(user)

		// const [categoryName, setcategoryName] = useState('');
		// const [categoryType, setcategoryType] = useState('');

		const [categories, setCategories] = useState([]);

		useEffect(() => {
			fetch(`${AppHelper.API_URL}/users/categories`, { 
				headers: 
				{
				 Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}) // From the backend.
			.then(AppHelper.toJSON)
			.then(data => {
				console.log(data)

				setCategories(data)

				setCategoryCards(data.map((category) => {
					return <Category key={category._id} category={category} />
				})
				)

			})
				
		}, [])

	useEffect(() => {
		let updatedCategories = categories.map((category) => {
			if((category.categoryType === categorysearchType) || (categorysearchType === "All")) {
				if (categorySearch !== null) {
						if(category.categoryName.includes(categorySearch)) {
							return <Category key={category._id} category={category} />
						}
				}
				else {
					return <Category key={category._id} category={category} />
				}
			}
			else {
				return null
			}
		})
		setCategoryCards(updatedCategories)

	}, [categorySearch, categorysearchType])

	function filter(e) {
			e.preventDefault();

			const options = {
				method: "POST",
				headers: 
				{
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			}
			fetch(`${ AppHelper.API_URL}/users/categories`, options)
			.then(AppHelper.toJSON)
			.then(data => {
				setCategories(data)
			})
		}
		
	return(
		<React.Fragment>
		<Head>
				<title> My Categories | Budgeteer </title>
		</Head>
		{
		(user.id === null) ? 
			<Container className="mt-5 pt-5 mb-5"> >
				<Alert show={show} variant="danger">
			        <Alert.Heading>An error occurred!</Alert.Heading>
			        <p>
			          You are not logged in to access this page!
			        </p>
			        <hr />
			        <div className="d-flex justify-content-end">
			          <Button onClick={handleClick} variant="outline-success">
			            Login
			          </Button>
			        </div>
			    </Alert>
			</Container>
		:
		<React.Fragment>
			<Container className="mt-5 pt-5 mb-5">
				<h1> Categories </h1>
				<Row>
				<Col xs md="6">
				<Button variant="success" type="submit" id="addBtn" onClick={() => setModalShow(true)}>
				<FontAwesomeIcon icon={faPlus} /> Add New Category </Button>
			      <AddNewCategory
			        show={modalShow}
			        onHide={() => setModalShow(false)}
			      />
			    </Col>
			    </Row>
			    <Form onSubmit={(e) => filter(e)}>
					<Row>
						<Col xs md="4">
					        <Form.Group controlId="categorySearch">
						        <Form.Label>Category Search: </Form.Label>
						        <Form.Control
						          type="text"
						          placeholder="Enter Category Name"
						          onChange={(e) => setcategorySearch(e.target.value)}
						        />
						    </Form.Group>
						</Col>
						<Col xs md="4">
						    <Form.Group controlId="categoryType">
								<Form.Label>Category Type: </Form.Label>
							    	<Dropdown onSelect={handleSelect}> 
										<Dropdown.Toggle id="dropdown">
											{categorysearchType}
										</Dropdown.Toggle>
										<Dropdown.Menu>
								    		<Dropdown.Item eventKey="All">All</Dropdown.Item>
								    		<Dropdown.Item eventKey="Income">Income</Dropdown.Item>
								    		<Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
								  		</Dropdown.Menu>
									</Dropdown>
					        </Form.Group>
			   			</Col>   			
					</Row>
					</Form>
			    <Row>
			    <Col xs md="12">
					{(categories.length > 0) ? 
						<React.Fragment>
						<Table striped bordered hover responsive>
						<thead>
							<tr>
								<th>Category Name</th>
								<th>Category Type</th>
								<th className="text-center">Actions</th>
							</tr>
						</thead>
						<tbody>
						{categoryCards}
						</tbody>
						</Table>
						</React.Fragment>
					:
						<Alert variant="info">
			            	You currently have no categories!
			            </Alert> 
					}
				</Col>
				</Row>
			</Container>
		</React.Fragment>
		}
		</React.Fragment>
		)
}

function AddNewCategory(props) {
	
	const [categoryName, setcategoryName] = useState('')
	const [categoryType, setcategoryType] = useState('Income')

		function addNew(e) {
			e.preventDefault();
			const options = { 
				method: "POST",
				headers: 
				{
				 'Content-Type': 'application/json',
				 Authorization: `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify(
				{
					categoryName: categoryName,
					categoryType: categoryType
				})
			}
			fetch(`${AppHelper.API_URL}/users/categories/new`, options) // From the backend.
			.then(AppHelper.toJSON)
			.then(data => {
				console.log(data)
				if(data === true) {
				Swal.fire({
					icon: 'success',
					title: `You have successfully created a new ${newrecordType} record!`,
				}).then(() => {
					router.reload('/categories')
				})
			}
			})
		}
		
		const handleSelect=(e)=>{
    		setcategoryType(e)
 		}
	  return (
	  	<React.Fragment>
	    <Modal
	      {...props}
	      size="lg"
	      aria-labelledby="contained-modal-title-vcenter"
	      centered
	      border="primary"
		  className="mb-2"

	    >
	    <Form onSubmit={(e) => addNew(e)}>
	    <Modal.Header closeButton>
	    	<Modal.Title id="contained-modal-title-vcenter">
	        	Add New Category
	        </Modal.Title>
	    </Modal.Header>
	    <Modal.Body>
	        	<Col xs md="6">
				    <Form.Group controlId="categoryName">
					    <Form.Label>Category Name: </Form.Label>
					    <Form.Control
					        type="text"
					        placeholder="Enter Category Name"
					        onChange={(e) => setcategoryName(e.target.value)}
					    />
					</Form.Group>
				</Col>
				<Col xs md="6">
					<Form.Group controlId="categoryType">
						<Form.Label>Category Type: </Form.Label>
						<Dropdown onSelect={handleSelect}> 
							<Dropdown.Toggle variant="success" id="dropdown">
								{categoryType}
							</Dropdown.Toggle>
							<Dropdown.Menu>
							    <Dropdown.Item eventKey="Income">Income</Dropdown.Item>
							    <Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
							</Dropdown.Menu>
						</Dropdown>
				    </Form.Group>
		   		</Col>
	    </Modal.Body>
	    <Modal.Footer>
	    	<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
	    </Modal.Footer>
	    </Form>
	    </Modal>
	    </React.Fragment>
	  );
	}
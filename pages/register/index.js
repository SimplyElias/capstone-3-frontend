import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router'
import Head from "next/head"
import { Form, Button, Container } from 'react-bootstrap';
import AppHelper from '../../app-helper'
import { Row, Col, Card } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';

export default function index() {
  return (
    <React.Fragment>
    <Head>
      <title> Register | Budgeteer </title>
    </Head>
    <Container className="mt-5 pt-5 mb-5">
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Register</h3>
          <RegisterForm />
        </Col>
      </Row>
    </Container>
    </React.Fragment>
    )
}


const RegisterForm = () => {

  const {user, setUser} = useContext(UserContext)

  const [firstName, setfirstName] = useState('');
  const [lastName, setlastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNum, setmobileNum] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState(false);


  function register(e) {
    e.preventDefault();

    const options = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json'},
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNum,
        password: password
      })
    }

    fetch(`${AppHelper.API_URL}/users`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      if (data === true) {
        Swal.fire('Registered!', 'You are registered! Please login!', 'success')
        Router.push("/login")
        console.log(data)
        //  localStorage.setItem('email', email);
        //  localStorage.setItem('isAdmin', match.isAdmin);
      }
    })

    

  //   useEffect(() => {

  //   if(firstName !== "") {
  //     console.log("FNAME")
  //   }
  //   else if (lastName !== "") {
  //     console.log("LNAME")
  //   }
  //   else if (email !== "") {
  //     console.log("LNAMEmil")
  //   }
  //   else if (mobileNum !== "") {
  //     console.log("LNAMEmob")
  //   }
  //   else if (password !== "") {
  //     console.log("LNasd")
  //   }

  //   if(firstName !== "" && lastName !== "" && email !== "" && mobileNum !== "" && password !== "") {
  //     console.log(isActive)
  //     setIsActive(true)
  //   } else {
  //     console.log(isActive)
  //     setIsActive(false)
  //   }
  // }, [firstName, lastName, email, mobileNum, password])
    // const match = usersData.find(user => {
    //  return(user.email === email && user.password === password) 
    // })

    // if(match){
    //  console.log(match)

    //  localStorage.setItem('email', email);
    //  localStorage.setItem('isAdmin', match.isAdmin);

    //  setUser({
    //    email: localStorage.getItem('email'),
    //    isAdmin: match.isAdmin
    //  })

    //  setEmail('');
    //  setPassword('');

    //  Router.push('/courses');
    // }
    // else {
    //  alert('Auth failed.')
    // }
  }

    const authenticateGoogleToken = (response) => {
    console.log(response)

    const payload = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({ tokenId: response.tokenId})
    }

    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
    .then(AppHelper.toJSON)
    .then(data => {
      if (typeof data.accessToken !== 'undefined') {
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
        //  localStorage.setItem('email', email);
        //  localStorage.setItem('isAdmin', match.isAdmin);
      }
      else {
        if (data.error == 'google-auth-error') {
          Swal.fire(
            'Google Auth Error',
            'Google Authentication procedure Failed',
            'error')
        }
        else if (data.error === 'login-type-error') {
          Swal.fire(
            'Login Type Error',
            'Login procedure failed!',
            'error')
        }
      }
    })
  }

  const failed = (response) => {
    console.log(response)
  }

  const retrieveUserDetails = (accessToken) => {
    const options = { 
      headers: { Authorization: `Bearer ${ accessToken }`}
    }

    fetch(`${AppHelper.API_URL}/users/details`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      Swal.fire(
        'Success',
        'You have successfully logged in!',
        'success')
      setUser({ id: data._id, isAdmin: data.isAdmin, email: data.email})
      Router.push('/');
    })
  }


    return (
  <React.Fragment>
    <Form onSubmit={(e) => register(e)}>
    <Row>
    <Col xs md="6">
        <Form.Group controlId="firstName">
        <Form.Label>First name: </Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter First Name"
          value={firstName}
          onChange={(e) => setfirstName(e.target.value)}
          required
        />
        </Form.Group>
    </Col>
    <Col xs md="6">
        <Form.Group controlId="lastName">
        <Form.Label>Last name: </Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter Last Name"
          value={lastName}
          onChange={(e) => setlastName(e.target.value)}
          required
        />
        </Form.Group>
    </Col>
    </Row>
    <Row>
    <Col xs md="12">
        <Form.Group controlId="email">
        <Form.Label>Email: </Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email Address"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        </Form.Group>
    </Col>
    </Row>
    <Row>
    <Col xs md="12">
        <Form.Group controlId="mobileNum">
        <Form.Label>Mobile Number: </Form.Label>
        <Form.Control
          type="number"
          placeholder="Enter Mobile Number"
          value={mobileNum}
          onChange={(e) => setmobileNum(e.target.value)}
          required
        />
        </Form.Group>
    </Col>
    </Row>
    <Row>
    <Col xs md="12">
        <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
      </Form.Group>
    </Col>
    </Row>
      <Row>
      <Button variant="primary" type="submit" id="submitBtn" block>Submit</Button>
      </Row>
      <hr />
    <p className="text-center"> You can also register using Google! </p> 
      <GoogleLogin 
        //clienrId = OAuthClient id from Google Clound
        clientId="692172475602-aa7p7m5it9q2hvo7e9d674u1p8ccf8s7.apps.googleusercontent.com"
        buttonText="Login"

        onSuccess={ authenticateGoogleToken }
        onFailure={ failed }


        cookiePolicy={ 'single_host_origin'}
        className="w-100 text-center d-flex justify-content-center" />
    </Form>
  </React.Fragment>
    )
}
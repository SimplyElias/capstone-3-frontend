import Head from 'next/head'
import React, { useContext, useState, useEffect } from 'react'
import styles from '../styles/Home.module.css'
import { Container, Jumbotron, Carousel, Button, Row, Col, Card, Nav, Navbar, Alert, Tab, Tabs} from 'react-bootstrap'
import UserContext from '../UserContext'
import Image from 'react-bootstrap/Image'
import AppHelper from '../app-helper'
import moment from 'moment'
import BudgetTrend from '../components/BudgetTrend'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faEdit, faTrash, faBook, faPoll, faUser} from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function Home() {

  const { user } = useContext(UserContext);


  return (
    <React.Fragment>
    <Head>
    	<title> Budgeteer </title>
    </Head>
    {(user.id !== null) ?
      <React.Fragment>
      <Container className="mt-5 pt-5 mb-5">
      <GetDetails />
      </Container>
      </React.Fragment>
      :
      <React.Fragment>
      <Jumbotron fluid>
        <Container>
          <h1>Welcome to Budgeteer!</h1>
          <p>
            Manage your income and expenses here! Sign up now!
          </p>
          <Button href="/register"> Sign up! </Button>
        </Container>
      </Jumbotron>
        <Container className="mt-5 pt-5 mb-5">
          <Row>
            <Col>
              <Image src="images/budget-1.png/" className="img-thumbnail" roundedCircle fluid/>
            </Col>
            <Col className="text-middle align-text-bottom">
              <h1>Keep track of your budget!</h1>
              <p> Budgeteer watches over your income and expenses at ease! </p>
            </Col>
          </Row>
          <Row>
            <Col className="text-middle align-text-bottom">
              <h1>Manage your finances!</h1>
              <p> Having trouble being organized? Budgeteer has what it takes to organize your small savings in budget! </p>
            </Col>
            <Col>
              <Image src="images/budget-2.jpg/" roundedCircle fluid/>      
            </Col>
          </Row>
        </Container>
      <Container className="mt-5 pt-5 mb-5">
        <footer className="footer">
        <div className="container">
          <span className="text-muted">All images and data in this application are for educational purposes only.</span>
        </div>
      </footer>
      </Container>
      </React.Fragment>
    }
    
    </React.Fragment>
  )
}

function GetDetails () {

  const [firstName, setfirstName] = useState('')
  const [balance, setBalance] = useState(0)
  const [latestRecord, setlatestRecord] = useState([])
  const [latestRecordType, setlatestRecordType] = useState('')
  const [latestRecordedOn, setlatestRecordedOn] = useState('')
  const [noRecord, setnoRecord] = useState(false)
  const [recordData, setrecordData] = useState([])
  
  useEffect(() => {
      fetch(`${AppHelper.API_URL}/users/details`, { 
        headers: 
        {
         Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }) // From the backend.
      .then(AppHelper.toJSON)
      .then(data => {
        console.log(data)
        console.log(data.balance)
        setfirstName(data.firstName)
        setBalance(parseInt(data.balance))
        let latestRecordData = []
        let allrecordData = []

          if(data.records.length > 0) {
            
          latestRecordData = data.records[data.records.length - 1]
          if (latestRecordData.recordType === "Income") {
            setlatestRecordType('Income')
          }
          else {
            setlatestRecordType('Expense')
          }
        }
        else{
          setnoRecord(true)
        }

        // data.records.map((record) => {
        //   latestRecordData.push(record.indexOf(record.length - 1))
        // })
        setrecordData(data.records)
        setlatestRecordedOn(moment(latestRecordData.recordedOn).format('yyyy-MM-DD'))
        setlatestRecord(latestRecordData)
        console.log(latestRecordData)
      })
        
    }, []) 


  return(
    <React.Fragment>
    <h1> Welcome, {firstName}! </h1>
    <br />
      <Tab.Container id="left-tabs-example" defaultActiveKey="overview">
        <Row>
          <Col sm={3}>
            <Nav variant="pills" className="flex-column">
              <Nav.Item>
                <Nav.Link eventKey="overview">Overview</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="stats">Stats</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col sm={9}>
            <Tab.Content>
              <Tab.Pane eventKey="overview">
                  <h2>Overview</h2>
                  <h3 className="text-right">
                    Balance: ₱{balance}
                  </h3>
                  {(noRecord === false) ?
                    <React.Fragment>
                    {(latestRecord.recordType === "Income") ?
                    <Card.Text as="h6"> Your last record at <b> {latestRecordedOn} </b> was an <b className="text-success">{latestRecordType}</b> record. <a href="/records"> → View records </a> </Card.Text>
                    :
                    <Card.Text as="h6"> Your last record at <b> {latestRecordedOn} </b> was an <b className="text-danger">{latestRecordType}</b> record. <a href="/records"> → View records </a> </Card.Text>
                    }
                    </React.Fragment>
                    :
                    <React.Fragment>
                    <Alert variant="info">
                          You currently have no records. <a href="/records"> → View records </a>
                    </Alert>
                    </React.Fragment>
                  }
                  <Row className="justify-content-md-center justify-content-center">
                    <Button sm={8}  md="auto" className="center m-3" variant="primary" href="/categories">
                     <FontAwesomeIcon icon={faBook} /> View Categories</Button>
                    <Button sm={8} md="auto" className="center m-3" variant="info" href="/records">
                     <FontAwesomeIcon icon={faPoll} /> View Records</Button>
                    <Button sm={8} md="auto" className="center m-3" variant="warning" href="/profile">
                     <FontAwesomeIcon icon={faUser} /> View Profile</Button>
                  </Row>
              </Tab.Pane>
              <Tab.Pane eventKey="stats">
                <h2>Statistics</h2>
              {(noRecord === false) ?
                    <React.Fragment>
                    <BudgetTrend recordData={recordData}/>
                    </React.Fragment>
                    :
                    <React.Fragment>
                    <Alert variant="info">
                          You currently have no data to render. Please check your records. <a href="/records"> → View records </a>
                    </Alert>
                    </React.Fragment>
                  }
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </React.Fragment>
  )
}

import React, { useState, useEffect } from 'react'
import Router from 'next/router'
import Head from "next/head"
import AppHelper from '../../app-helper'
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import Tab from 'react-bootstrap/Tab'
import Nav from 'react-bootstrap/Nav';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';
import Profile from '../../components/Profile';
import ChangePassword from '../../components/Password';
import DeleteData from '../../components/DeleteData';

export default function index() {

	const [profileData, setProfileData] = useState([]);
	const [firstName, setFirstName] = useState('');


	useEffect(() => {

		fetch(`${AppHelper.API_URL}/users/details`, { 
			headers: 
			{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			setProfileData(data)
			setFirstName(data.firstName)
		})
	}, [])


	return(
		<React.Fragment>
		<Head>
			<title> My Account </title>
		</Head>
			<Container className="mt-5 pt-5 mb-5">
			<h1> My Account </h1>
			<p> Navigate through your profile using these tabs! </p>
			<Tab.Container id="left-tabs-example" defaultActiveKey="profile">
			  <Row>
			    <Col sm={3}>
			      <Nav variant="pills" className="flex-column">
			        <Nav.Item>
			          <Nav.Link eventKey="profile"><b>Profile</b></Nav.Link>
			        </Nav.Item>
			        <Nav.Item>
			          <Nav.Link eventKey="settings"><b>Account Settings</b></Nav.Link>
			        </Nav.Item>
			      </Nav>
			    </Col>
			    <Col sm={9}>
			      <Tab.Content>
			        <Tab.Pane eventKey="profile">
			        	<h1> My Profile </h1>
			        	<Profile profileData={profileData} />
			        </Tab.Pane>
			        <Tab.Pane eventKey="settings">
			        	<h1> Account Settings </h1>
			        	<Col className="mt-3 pt-3 mb-3">
			        	<ChangePassword />
			        	</Col>
			        	<Col className="mt-3 pt-3 mb-3">
			        	<DeleteData />
			        	</Col>
			        </Tab.Pane>
			      </Tab.Content>
			    </Col>
			  </Row>
			</Tab.Container>
			</Container>
		</React.Fragment>
		)
}
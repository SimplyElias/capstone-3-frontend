import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head'
import Router from 'next/router'
import { Form, Button, Container } from 'react-bootstrap';
import AppHelper from '../../app-helper'
import { Row, Col, Card } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2'
import UserContext from '../../UserContext';

export default function index() {
	return(
	<React.Fragment>
		<Head>
			<title> Login | Budgeteer </title>
		</Head>
		<Container className="mt-5 pt-5 mb-5">
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Login</h3>
					<LoginForm/>
				</Col>
			</Row>
		</Container>
	</React.Fragment>
		)
}

const LoginForm = () => {

	const { user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	// function loginUser(e) {

	// 	e.preventDefault();

	// 	setEmail('');
	// 	setPassword('');

	// 	console.log(email + " has been logged in! Thank you!")

	// }

	function authenticate(e) {
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}
		fetch(`${ AppHelper.API_URL}/users/login`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					icon: 'success',
					title: 'You will be redirected shortly...',
					showConfirmButton: false,
					timer: 1500
				})
			} else {
				if(data.error === 'does-not-exist') {
					Swal.fire('Authentication failed!', 'The account registered with this email does not exist!', 'error')
				}
				else if (data.error === 'incorrect-password') {
					Swal.fire('Authentication failed!', 'The password registered with this email is incorrect!', 'error')
				}
				else if(data.error === 'login-type-error') {
					Swal.fire('Authentication failed!', 'An error occurred. Please contact administrator!', 'error')
				}
			}

		})
		// const match = usersData.find(user => {
		// 	return(user.email === email && user.password === password) 
		// })

		// if(match){
		// 	console.log(match)

		// 	localStorage.setItem('email', email);
		// 	localStorage.setItem('isAdmin', match.isAdmin);

		// 	setUser({
		// 		email: localStorage.getItem('email'),
		// 		isAdmin: match.isAdmin
		// 	})

		// 	setEmail('');
		// 	setPassword('');

		// 	Router.push('/courses');
		// }
		// else {
		// 	alert('Auth failed.')
		// }
	}

	useEffect(() => {
		// Validation
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])


	const authenticateGoogleToken = (response) => {
		console.log(response)

		const payload = {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({ tokenId: response.tokenId})
		}

		fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
		.then(AppHelper.toJSON)
		.then(data => {
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
				Router.push("/");
			}
			else {
				if (data.error == 'google-auth-error') {
					Swal.fire(
						'Google Auth Error',
						'Google Authentication procedure Failed',
						'error')
				}
				else if (data.error === 'login-type-error') {
					Swal.fire(
						'Login Type Error',
						'Login procedure failed!',
						'error')
				}
			}
		})
	}

	const failed = (response) => {
		console.log(response)
	}

	const retrieveUserDetails = (accessToken) => {
		const options = { 
			headers: { Authorization: `Bearer ${ accessToken }`}
		}

		fetch(`${ AppHelper.API_URL}/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			localStorage.setItem('id', data._id);
			localStorage.setItem('isAdmin', data.isAdmin);
			localStorage.setItem('firstName', data.firstName);
			setUser({ id: data._id, isAdmin: data.isAdmin, firstName: data.firstName})
			Router.push('/');
		})
	}

	return (
	<React.Fragment>
		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>


			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive ? <Button variant="primary" type="submit" id="submitBtn">
				Submit
			</Button>
			:
			<>
			<Button variant="danger" type="submit" id="submitBtn" disabled>
				Submit
			</Button>

			<GoogleLogin 
				//clienrId = OAuthClient id from Google Clound
				clientId="692172475602-aa7p7m5it9q2hvo7e9d674u1p8ccf8s7.apps.googleusercontent.com"
				buttonText="Login"

				onSuccess={ authenticateGoogleToken }
				onFailure={ failed }


				cookiePolicy={ 'single_host_origin'}
				className="w-100 text-center d-flex justify-content-center" />
				</>
			}
			
		</Form>
	</React.Fragment>
		)
}
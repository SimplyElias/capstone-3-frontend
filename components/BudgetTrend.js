import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { Form, Button, Row, Col, Alert, Dropdown, Container } from 'react-bootstrap'
import AppHelper from '../app-helper'
import Swal from 'sweetalert2'
import { Line } from 'react-chartjs-2';
import moment from 'moment'

export default function BudgetTrend({recordData}) {
    const [recordFiltered, setrecordFiltered] = useState([])
    const [startDate, setstartDate] = useState('')
    const [endDate, setendDate] = useState('')
    const [recordType, setrecordType] = useState('All')
    const [dateData, setDateData] = useState([])
    const [amountData, setAmountData] = useState([])

    useEffect(() => {

        let dateArray = []
        let amountArray = []
        let currDate = new Date()

        recordData.forEach((record) => {
            dateArray.push(moment(record.recordedOn).format('yyyy-MM-DD'))
            amountArray.push(record.recordAmount)
        })
        setDateData(dateArray)
        setAmountData(amountArray)
        
    }, [recordData])

    const data =  {
        labels: dateData,
        datasets: [
        { 
            label: 'Budget Trend',
            data: amountData,
            backgroundColor: 'rgba(63, 127, 191, 0.9)',
        }
        ],

    }

        return (
        <React.Fragment>
            <Container>

            <h6> Overview Budget Trend <a href="/charts/budget-trend"> → View Budget Trend </a></h6>
            </Container>

            { recordData.length > 0 
                ?
                <Row>
                    <Col md={12}>
                        <Line data={data} redraw={true}/>
                    </Col>
                </Row>
                :
                    <Alert variant="info">
                        No data found.
                    </Alert>       
            }
        </React.Fragment>
    )
}
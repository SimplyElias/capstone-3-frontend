import React, { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import { Button, Alert } from 'react-bootstrap'
import { useRouter } from 'next/router'

export default function Monthlyincome({incomeData}) {
	const router = useRouter()
	const [show, setShow] = useState(true);
	const handleClick = (e) => {
    e.preventDefault()
    router.reload("/records")
 	}
	// States to store the info in the bar chart
	const [months, setMonths] = useState([]);
	const [monthlyIncome, setMonthlyIncome] = useState([]);

	useEffect(() => {

		if(incomeData.length > 0) {

			let tempMonths = []

			incomeData.forEach(element =>	{
				if(!tempMonths.find(month => month === moment(element.recordedOn).format('MMMM') )) {
					tempMonths.push(moment(element.recordedOn).format('MMMM'))
				}
			})
			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

				// Will sort the tempMonth array referncing array
				// 'a' = "January"
				// monthsRef.indexOf("January") = 0
			tempMonths.sort((a, b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {

					return monthsRef.indexOf(a) - monthsRef.indexOf(b);

				}
			})

			console.log(tempMonths)
			setMonths(tempMonths)
		}

	}, [incomeData])

	useEffect(() => {

		setMonthlyIncome(months.map(month => {
			let sales = 0;

			incomeData.forEach(element => {
				if(month === moment(element.recordedOn).format('MMMM')) {
					sales = sales + parseInt(element.recordAmount);
				}
			})

			return sales;

		}))
	}, [months])

	console.log(monthlyIncome)

	const data = {
		labels: months,
		datasets: [
		{
			label: 'Monthly incomes for 2021',
			backgroundColor: 'rgba(109, 196, 109, 0.45)',
			borderColor: 'rgba(9, 102, 9, 0.73)',
			borderWidth:  1,
			hoverBackgroundColor: 'rgba(100, 195, 100, 0.86)',
			hoverBorderColor: 'rgba(90, 230, 90, 0.73)',
			data: monthlyIncome
		}]
	}

	return(
		<React.Fragment>
		{(incomeData.length > 0) ?
			<Bar data={data} />
		:
			<Alert show={show} variant="info">
		        <Alert.Heading>No records!</Alert.Heading>
		        <p>
		          You currently have no income records.
		        </p>
		        <hr />
		        <div className="d-flex justify-content-end">
		          <Button onClick={handleClick} variant="outline-info">
		            Go to Records
		          </Button>
		        </div>
		    </Alert>
		
		}
		</React.Fragment>
	)
}
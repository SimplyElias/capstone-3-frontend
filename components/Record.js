import React, { useState, useEffect, useContext } from 'react'
import Router from 'next/router'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../app-helper'
import { Container, Row, Col, Card, Button, Alert, Dropdown, Form, Table } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons"; // import the icons you need
import moment from 'moment'

export default function Record({record}) {
	const router = useRouter()

	const [editModalShow, seteditModalShow] = React.useState(false);
	const [deleteModalShow, setdeleteModalShow] = React.useState(false);

	return (
		<React.Fragment>
		<tr>
			<td>{moment(record.recordedOn).format('YYYY/MM/DD HH:MM:SS')}</td>
			<td>{record.recordDescription}</td>
			<td>{record.recordCategory}
				{(record.recordType === "Income") ?
				<b className="text-success"> ({record.recordType}) </b>
				:
				<b className="text-danger"> ({record.recordType}) </b>
				}
			</td>
			<td>₱{record.recordAmount}
				{(record.recordType === "Income") ?
				<b className="text-success"> (+₱{record.balanceAfterRecord}) </b>
				:
				<b className="text-danger"> (-₱{record.balanceAfterRecord}) </b>
				}
			</td>
	    	<td className="text-center">
	    	<Button size="sm"variant="info" type="submit" id="editBtn" onClick={() => seteditModalShow(true)}>
	    	<FontAwesomeIcon icon={faEdit} /> Edit</Button>
		      <EditRecord
		        show={editModalShow}
		        onHide={() => seteditModalShow(false)}
		        record={record}
		      />
		    <Button size="sm" variant="danger" type="submit" id="deleteBtn" onClick={() => setdeleteModalShow(true)}> 
		    <FontAwesomeIcon icon={faTrash} /> Delete</Button>
		      <DeleteRecord
		        show={deleteModalShow}
		        onHide={() => setdeleteModalShow(false)}
		        record={record}
		      />
			</td>
		</tr>
		</React.Fragment>
	)
}

function EditRecord(props) {
	const router = useRouter()
	const [editModalShow, seteditModalShow] = React.useState(true);

	const [recordId, setcategoryId] = useState(props.record._id)
	const [recordDescription, setrecordDescription] = useState(props.record.recordDescription)
	const [dropdownCategory, setDropdownCategory] = useState(props.record.recordCategory)
	const [dropdownType, setDropdownType] = useState(props.record.recordType)
	const [recordType, setrecordType] = useState(props.record.recordType)
	const [recordCategories, setRecordCategories] = useState([])
	const [recordCategory, setrecordCategory] = useState(props.record.recordCategory)
	const [recordAmount, setrecordAmount] = useState(parseInt(props.record.recordAmount))

	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/categories`, { 
			headers: 
			{
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			setRecordCategories(data.map((category) => {
				if (category.categoryType === dropdownType) {
					return <Dropdown.Item key={category._id} eventKey={category.categoryName}> {category.categoryName}</Dropdown.Item>
				}	
			})
			)
		})
	}, [dropdownType])

	function editExisting(e) {
		e.preventDefault();
		const options = { 
			method: "PUT",
			headers: 
			{
			 'Content-Type': 'application/json',
			 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			{
				recordId: recordId,
				recordDescription: recordDescription,
				recordType: recordType,
				recordCategory: recordCategory,
				recordAmount: parseInt(recordAmount)
			})
		}
		fetch(`${AppHelper.API_URL}/users/records/edit`, options) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					icon: 'success',
					title: `You have successfully edited the record with ID: ${recordId}!`,
				}).then(() => {
					router.reload('/records')
				})
				seteditModalShow(false)
			}
			else if(data.error === "no-changes") {
				Swal.fire({
					icon: 'info',
					title: `You made no changes to the record with ID: ${recordId}!`,
				}).then(() => {
					router.reload('/records')
				})
				seteditModalShow(false)
			}
		})
	}
		
	const edithandleSelect=(e)=>{
		console.log(e)
		setDropdownType(e)
		setrecordType(e)
	}
	const edithandleCategorySelect=(e)=>{
		console.log(e)
		setrecordCategory(e)
		setDropdownCategory(e)
	}
	
	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/categories`, { 
			headers: 
			{
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		}) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {

			setRecordCategories(data.map((category) => {
				if (category.categoryType === recordType) {
					return <Dropdown.Item key={category._id} eventKey={category.categoryName}> {category.categoryName}</Dropdown.Item>
				}	
			})
			)

		})
	}, [recordType])

	return (
	  	<React.Fragment>
	    <Modal
	      {...props}
	      size="lg"
	      aria-labelledby="contained-modal-title-vcenter"
	      centered
	    >
	    <Form onSubmit={(e) => editExisting(e)}>
	    
	    <Modal.Header closeButton>
	    	<Modal.Title id="contained-modal-title-vcenter">
	        	Edit Record
	        </Modal.Title>
	    </Modal.Header>
	    <Modal.Body>
    		<Col xs md="6">
			    <Form.Group controlId="editcategoryId">
				    <Form.Label>Record Id: </Form.Label>
				    <Form.Control
				        type="text"
				        value={props.record._id}
				        onSubmit={(e) => setrecordId(e.target.value)}
				        readOnly
				    />
				</Form.Group>
			</Col>
        	<Col xs md="6">
			    <Form.Group controlId="editrecordDescription">
				    <Form.Label>Record Description: </Form.Label>
				    <Form.Control
				        type="text"
				        placeholder={props.record.recordDescription}
				        onChange={(e) => setrecordDescription(e.target.value)}
				        value={recordDescription}
				    />
				</Form.Group>
			</Col>
			<Col xs md="6">
				<Form.Group controlId="editcategoryType">
					<Form.Label>Record Type: </Form.Label>
					<Dropdown onSelect={edithandleSelect}> 
						<Dropdown.Toggle variant="success" id="dropdown">
							{dropdownType}
						</Dropdown.Toggle>
						<Dropdown.Menu>
						    <Dropdown.Item eventKey="Income">Income</Dropdown.Item>
						    <Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
						</Dropdown.Menu>
					</Dropdown>
			    </Form.Group>
	   		</Col>
	   		<Col xs md="6">
				<Form.Group controlId="editrecordCategory">
					<Form.Label>Record Category: </Form.Label>
					<Dropdown onSelect={edithandleCategorySelect}> 
						<Dropdown.Toggle variant="success" id="dropdown">
							{dropdownCategory}
						</Dropdown.Toggle>
						<Dropdown.Menu>
						    {(recordCategories.length > 0) ?
								recordCategories
							:
								<b> No categories </b>
							}
						</Dropdown.Menu>
					</Dropdown>
			    </Form.Group>
	   		</Col>
	   		<Col xs md="6">
			    <Form.Group controlId="editRecordAmount">
				    <Form.Label>Record Amount: </Form.Label>
				    <Form.Control
				        type="number"
				        placeholder={props.record.recordAmount}
				        onChange={(e) => setrecordAmount(e.target.value)}
				        value={recordAmount}
				    />
				</Form.Group>
			</Col>
	    </Modal.Body>
	    <Modal.Footer>
	    	<Button variant="primary" type="submit" id="submitBtn">Update</Button>
	      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
	    </Modal.Footer>
	    </Form>
	    </Modal>
	    </React.Fragment>
	);
}

function DeleteRecord(props) {

	const router = useRouter()
	const [deleteModalShow, setdeleteModalShow] = React.useState(false);

	const [recordId, setrecordId] = useState(props.record._id)
	const [recordDescription, setrecordDescription] = useState(props.record.recordDescription)

	function deleteExisting(e) {
		e.preventDefault();
		const options = { 
			method: "DELETE",
			headers: 
			{
			 'Content-Type': 'application/json',
			 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			{
				recordId: recordId,
			})
		}
		fetch(`${AppHelper.API_URL}/users/records/delete`, options) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					icon: 'success',
					title: `You have successfully deleted the record with ID: ${recordId}!`,
				}).then(() => {
					router.reload('/records')
				})
				setdeleteModalShow(false)
			}
		})
	}
	
return (

  	<React.Fragment>
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
    <Form onSubmit={(e) => deleteExisting(e)}>
    
    <Modal.Header closeButton>
    	<Modal.Title id="contained-modal-title-vcenter">
        	Deleting Record: {recordDescription}
        </Modal.Title>
    </Modal.Header>
    <Modal.Body>
    		<Col xs md="12">
			    <p> You are deleting the record: <b> {recordDescription} </b> </p>
			 	<p> Are you sure you want to delete this record? </p>
			 	<p className="text-danger"> <strong> This action cannot be undone. Are you sure? </strong> </p>
			</Col>
    </Modal.Body>
    <Modal.Footer>
    	<Button variant="danger" type="submit" id="submitBtn">
    	<FontAwesomeIcon icon={faTrash} /> Delete </Button>
      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
    </Modal.Footer>
    </Form>
    </Modal>
    </React.Fragment>
  );
}
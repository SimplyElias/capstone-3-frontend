import React, { useState, useEffect, useContext } from 'react'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../app-helper'
import { Container, Row, Col, Card, Button, Alert, Dropdown, Form, Table } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function Category({category}) {
	const router = useRouter()
	// const categoryId = category.categoryId

	const [editModalShow, seteditModalShow] = React.useState(false);
	const [deleteModalShow, setdeleteModalShow] = React.useState(false);

	// function TotalRecords(categoryName) {
	// 	const options = { 
	// 		method: "PUT",
	// 		headers: 
	// 		{
	// 			'Content-Type': 'application/json',
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		},
	// 		body: JSON.stringify(
	// 		{
	// 			categoryId: categoryId,
	// 			categoryName: categoryName,
	// 			categoryType: categoryType
	// 		})
	// 		}
	// }

	function EditCategory(props) {
	
		const [categoryId, setcategoryId] = useState(category._id)
		const [categoryName, setcategoryName] = useState(category.categoryName)
		const [categoryType, setcategoryType] = useState(category.categoryType)

			function editExisting(e) {
				e.preventDefault();
				const options = { 
					method: "PUT",
					headers: 
					{
					 'Content-Type': 'application/json',
					 Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify(
					{
						categoryId: categoryId,
						categoryName: categoryName,
						categoryType: categoryType
					})
				}
				console.log(categoryId + " AND " + categoryName + " AND " + categoryType)
				console.log(options)
				fetch(`${AppHelper.API_URL}/users/categories/edit`, options) // From the backend.
				.then(AppHelper.toJSON)
				.then(data => {
					console.log(data)
					if(data === true) {
						Swal.fire({
							icon: 'success',
							title: `You have edited the category ${categoryName}!`,
						}).then(() => {
							router.reload('/categories')
						})
						seteditModalShow(false)
					}
					else if(data.error === "no-changes") {
						Swal.fire({
							icon: 'info',
							title: `You made no changes to ${categoryName}!`,
						}).then(() => {
							router.reload('/categories')
						})
						seteditModalShow(false)
					}
					else if(data.error === "category-none") {
						Swal.fire({
							icon: 'info',
							title: `An error occurred while updating ${categoryName}!`,
						}).then(() => {
							router.reload('/categories')
						})
						seteditModalShow(false)
					}
				})
			}
			
			const edithandleSelect=(e)=>{
	    		setcategoryType(e)
	 		}
		  return (

		  	<React.Fragment>
		    <Modal
		      {...props}
		      size="lg"
		      aria-labelledby="contained-modal-title-vcenter"
		      centered
		    >
		    <Form onSubmit={(e) => editExisting(e)}>
		    
		    <Modal.Header closeButton>
		    	<Modal.Title id="contained-modal-title-vcenter">
		        	Editing the Category: {category.categoryName}
		        </Modal.Title>
		    </Modal.Header>
		    <Modal.Body>
		    		<Col xs md="6">
					    <Form.Group controlId="editcategoryId">
						    <Form.Label>Category Id (Not editable): </Form.Label>
						    <Form.Control
						        type="text"
						        value={category._id}
						        onSubmit={(e) => setcategoryId(e.target.value)}
						        readOnly
						    />
						</Form.Group>
					</Col>
		        	<Col xs md="6">
					    <Form.Group controlId="editcategoryName">
						    <Form.Label>Category Name: </Form.Label>
						    <Form.Control
						        type="text"
						        placeholder={category.categoryName}
						        onChange={(e) => setcategoryName(e.target.value)}
						        value={categoryName}
						    />
						</Form.Group>
					</Col>
					<Col xs md="6">
						<Form.Group controlId="editcategoryType">
							<Form.Label>Category Type: </Form.Label>
							<Dropdown onSelect={edithandleSelect}> 
								<Dropdown.Toggle variant="success" id="dropdown">
									 {categoryType}
								</Dropdown.Toggle>
								<Dropdown.Menu>
								    <Dropdown.Item eventKey="Income">Income</Dropdown.Item>
								    <Dropdown.Item eventKey="Expense">Expense</Dropdown.Item>
								</Dropdown.Menu>
							</Dropdown>
					    </Form.Group>
			   		</Col>
		    </Modal.Body>
		    <Modal.Footer>
		    	<Button variant="primary" type="submit" id="submitBtn">
		    	<FontAwesomeIcon icon={faEdit} /> Update</Button>
		      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
		    </Modal.Footer>
		    </Form>
		    </Modal>
		    </React.Fragment>
		  );
		}

	function DeleteCategory(props) {
	
		const [categoryId, setcategoryId] = useState(category._id)
		const [categoryName, setcategoryName] = useState(category.categoryName)

			function deleteExisting(e) {
				e.preventDefault();
				const options = { 
					method: "DELETE",
					headers: 
					{
					 'Content-Type': 'application/json',
					 Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify(
					{
						categoryId: categoryId,
					})
				}
				fetch(`${AppHelper.API_URL}/users/categories/delete`, options) // From the backend.
				.then(AppHelper.toJSON)
				.then(data => {
					console.log(data)
					if(data === true) {
						Swal.fire({
							icon: 'success',
							title: `You have deleted the category ${categoryName}!`,
						}).then(() => {
							router.reload('/categories')
						})
						seteditModalShow(false)
					}
					else {
						Swal.fire({
							icon: 'info',
							title: `An error occurred while deleteing ${categoryName}!`,
						}).then(() => {
							router.reload('/categories')
						})
						seteditModalShow(false)
					}
				})
			}
			
		  return (

		  	<React.Fragment>
		    <Modal
		      {...props}
		      size="lg"
		      aria-labelledby="contained-modal-title-vcenter"
		      centered
		    >
		    <Form onSubmit={(e) => deleteExisting(e)}>
		    
		    <Modal.Header closeButton>
		    	<Modal.Title id="contained-modal-title-vcenter">
		        	Deleting Category
		        </Modal.Title>
		    </Modal.Header>
		    <Modal.Body>
		    		<Col xs md="12">
					    <p> You are about to delete the category: <b> {categoryName} </b> </p>
					 	<p> Are you sure you want to delete this category? </p>
					 	<p className="text-danger"> <strong> All records associated with this category will be deleted. This action cannot be undone. </strong> </p>
					</Col>
		    </Modal.Body>
		    <Modal.Footer>
		    	<Button variant="danger" type="submit" id="submitBtn">
		    	<FontAwesomeIcon icon={faTrash} />Delete</Button>
		      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
		    </Modal.Footer>
		    </Form>
		    </Modal>
		    </React.Fragment>
		  );
		}

	return (
		<React.Fragment>
			<tr>
				<td>{category.categoryName}</td>
				<td>{category.recordCategory}
					{(category.categoryType === "Income") ?
					<b className="text-success">{category.categoryType}</b>
					:
					<b className="text-danger"> {category.categoryType} </b>
					}
				</td>
		    	<td className="text-center">
		    	<Button size="sm"variant="info" type="submit" id="editBtn" onClick={() => seteditModalShow(true)}>
		    	<FontAwesomeIcon icon={faEdit} /> Edit</Button>
			      <EditCategory
			        show={editModalShow}
			        onHide={() => seteditModalShow(false)}
			      />
			    <Button size="sm" variant="danger" type="submit" id="deleteBtn" onClick={() => setdeleteModalShow(true)}> 
			    <FontAwesomeIcon icon={faTrash} /> Delete</Button>
			      <DeleteCategory
			        show={deleteModalShow}
			        onHide={() => setdeleteModalShow(false)}
			      />
				</td>
			</tr>
		</React.Fragment>
	)
}
import React, {useContext} from 'react';
import Link from 'next/link'
// import React, { Component } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import UserContext from '../UserContext'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faEdit, faTrash, faBook, faPoll, faUser} from "@fortawesome/free-solid-svg-icons"; // import the icons you need

export default function NavBar() {

const { user } = useContext(UserContext);

  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Link href="/">
        <a className="navbar-brand" >Budgeteer</a>
      </Link>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="ml-auto">
          <Link href="/">
            <a className="nav-link" role="button">Home</a>
          </Link>
              {(user.id !== null) ?
            <React.Fragment>
          <Link href="/categories">
            <a className="nav-link" role="button">Categories</a>
          </Link>
          <Link href="/records">
            <a className="nav-link" role="button">Records</a>
          </Link>
          <Link href="/charts/monthly-income">
            <a className="nav-link" role="button">Montly Income</a>
          </Link>
          <Link href="/charts/monthly-expense">
            <a className="nav-link" role="button">Montly Expense</a>
          </Link>
          <Link href="/charts/category-breakdown">
            <a className="nav-link" role="button">Breakdown</a>
          </Link>
          <Link href="/charts/budget-trend">
            <a className="nav-link" role="button">Trend</a>
          </Link>
            <Link href="/profile">
              <a className="nav-link" role="button">Profile</a>
            </Link>
            <Link href="/logout">
              <a className="nav-link" role="button">Logout</a>
            </Link>
            </React.Fragment>
            :
            <React.Fragment>
              <Link href="/login">
              <a className="nav-link" role="button">Login</a>
              </Link>
              <Link href="/register">
              <a className="nav-link" role="button">Register</a>
              </Link>
            </React.Fragment>
          }
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )

}

// const express = require('express');

// export default class NaviBar extends Component {
//  render() {
//    return (
//      <Navbar bg="light" expand="lg">
//        <Navbar.Brand href="#home">Zuitter</Navbar.Brand>
//        <Navbar.Toggle aria-controls="basic-navbar-nav" />
//        <Navbar.Collapse id="basic-navbar-nav">
//          <Nav className="ml-auto">
//            <Nav.Link href="#home">Home</Nav.Link>
//            <Nav.Link href="#courses">Courses</Nav.Link>
//          </Nav>
//        </Navbar.Collapse>
//      </Navbar>
//    )
//  }
// }

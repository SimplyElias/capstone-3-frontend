import React, { useState, UseEffect } from 'react'
import Router from 'next/router'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../app-helper'
import { Container, Row, Col, Card, Image, Button, Form } from 'react-bootstrap'
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faTrash, faTimes } from "@fortawesome/free-solid-svg-icons"; // import the icons you need


export default function DeleteData() {
	const router = useRouter()

	const [deleteModalShow, setdeleteModalShow] = React.useState(false);
	function DeleteAllData(props) {
	
	const [currentPassword, setcurrentPassword] = useState('')

	function ClearData(e) {
		e.preventDefault();
		const options = { 
			method: "DELETE",
			headers: 
			{
			 'Content-Type': 'application/json',
			 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			{
				password: currentPassword,
			})
		}
		fetch(`${AppHelper.API_URL}/users/delete-data`, options) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			if(data === true) {
				Swal.fire({
					icon: 'success',
					title: `You have cleared all your data!`,
				}).then(() => {
					router.reload('/profile')
				})
				setdeleteModalShow(false)
			}
			else if(data.error === "incorrect-password") {
				Swal.fire({
					icon: 'error',
					title: `Your current password is incorrect!`,
				})
			}
			else {
				Swal.fire({
					icon: 'error',
					title: `An error occurred!`,
				})
			}
		})
	}
	
  return (

  	<React.Fragment>
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
    <Form onSubmit={(e) => ClearData(e)}>
		    
	    <Modal.Header closeButton>
	    	<Modal.Title id="contained-modal-title-vcenter">
	        	WARNING!
	        </Modal.Title>
	    </Modal.Header>
	    <Modal.Body>
	    		<Col xs md="12">
				    <p> You are about to delete <b> ALL </b> your budget information and transactions. </p>
				 	<p> This includes your categories, records, as well as your data in graphs.</p>
				 	<p className="text-danger"> <strong> All information on this account will be deleted. This action cannot be undone. Are you sure you want to clear your data?</strong> </p>
				 	<p> Please enter your password first. </p>
				</Col>
				<Col xs md="6">
				    <Form.Group controlId="currentPassword">
					    <Form.Label>Password: </Form.Label>
					    <Form.Control
					        type="password"
					        onChange={(e) => setcurrentPassword(e.target.value)}
					    />
					</Form.Group>
				</Col>
	    </Modal.Body>
	    <Modal.Footer>
	    	<Button variant="danger" type="submit" id="submitBtn">
	    	<FontAwesomeIcon icon={faTrash} /> Delete All Data</Button>
	      	<Button variant="light" onClick={props.onHide}>Cancel</Button>
	    </Modal.Footer>
	</Form>
    </Modal>
    </React.Fragment>
  );
}

	return (
		<React.Fragment>
		<Container>
			<Row>
			<Col>
			<Col xs={6} md={4}>
			</Col>
				<h4> Delete Information</h4>
				<p> You can delete all your information by pressing this button. </p>
				<p> Careful! This action cannot be undone! </p>
				<Button variant="warning" onClick={() => setdeleteModalShow(true)}>
				<FontAwesomeIcon icon={faTrash} /> Clear Data</Button>
				<DeleteAllData
				    show={deleteModalShow}
				    onHide={() => setdeleteModalShow(false)}
				/>
			</Col>
			</Row>
		</Container>
		</React.Fragment>
	)
}
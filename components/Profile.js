import React, { useState, UseEffect } from 'react'
import Router from 'next/router'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../app-helper'
import { Container, Row, Col, Card, Image, Button, Form } from 'react-bootstrap'
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faEdit, faTimes } from "@fortawesome/free-solid-svg-icons"; // import the icons you need


export default function Profile({profileData}) {
	const router = useRouter()

	const [editModalShow, seteditModalShow] = React.useState(false);
	console.log(profileData)
	function EditProfile (props) {
	
	const [userId, setuserId] = useState(profileData._id)
	const [firstName, setfirstName] = useState(profileData.firstName)
	const [lastName, setlastName] = useState(profileData.lastName)
	const [email, setEmail] = useState(profileData.email)
	const [mobileNo, setmobileNo] = useState(profileData.mobileNo)

	function editExisting(e) {
		e.preventDefault();
		const options = { 
			method: "PUT",
			headers: 
			{
			 'Content-Type': 'application/json',
			 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			{
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo
			})
		}
		fetch(`${AppHelper.API_URL}/users/edit`, options) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true) {
				router.reload("/profile")
			}
			else if(data.error === "no-changes") {
				Swal.fire({
					icon: 'info',
					title: `No changes were made to profile!`,
					showConfirmButton: true
				})
				seteditModalShow(false)
			}
		})
	}
	
  return (

  	<React.Fragment>
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
    <Form onSubmit={(e) => editExisting(e)}>
    
    <Modal.Header closeButton>
    	<Modal.Title id="contained-modal-title-vcenter">
        	Edit Profile
        </Modal.Title>
    </Modal.Header>
    <Modal.Body>
    		<Col xs md="6">
			    <Form.Group controlId="edituserId">
				    <Form.Label>User Id (Not editable): </Form.Label>
				    <Form.Control
				        type="text"
				        value={profileData._id}
				        onSubmit={(e) => setuserId(e.target.value)}
				        readOnly
				    />
				</Form.Group>
			</Col>
        	<Col xs md="6">
			    <Form.Group controlId="editfirstName">
				    <Form.Label>First Name: </Form.Label>
				    <Form.Control
				        type="text"
				        placeholder={profileData.firstName}
				        onChange={(e) => setfirstName(e.target.value)}
				        value={firstName}
				    />
				</Form.Group>
			</Col>
			<Col xs md="6">
				<Form.Group controlId="editlastName">
				    <Form.Label>Last Name: </Form.Label>
				    <Form.Control
				        type="text"
				        placeholder={profileData.firstName}
				        onChange={(e) => setlastName(e.target.value)}
				        value={lastName}
				    />
				</Form.Group>
	   		</Col>
	   		<Col xs md="6">
				<Form.Group controlId="editEmail">
				    <Form.Label>Email: </Form.Label>
				    <Form.Control
				        type="text"
				        placeholder={profileData.email}
				        onChange={(e) => setEmail(e.target.value)}
				        value={email}
				    />
				</Form.Group>
	   		</Col>
	   		<Col xs md="6">
				<Form.Group controlId="editlastName">
				    <Form.Label>Mobile Number: </Form.Label>
				    <Form.Control
				        type="text"
				        placeholder={profileData.mobileNo}
				        onChange={(e) => setmobileNo(e.target.value)}
				        value={mobileNo}
				    />
				</Form.Group>
	   		</Col>
    </Modal.Body>
    <Modal.Footer>
    	<Button variant="primary" type="submit" id="submitBtn">
    	<FontAwesomeIcon icon={faEdit} /> Update</Button>
      	<Button variant="warning" onClick={props.onHide}>
      	<FontAwesomeIcon icon={faTimes} /> Cancel</Button>
    </Modal.Footer>
    </Form>
    </Modal>
    </React.Fragment>
  );
}



	return (
		<React.Fragment>
		<Container>
			<Row>
			<Col>
			<h3> </h3>
			<Col xs={6} md={4}>
			<Image src="images/user.png/" roundedCircle fluid/>
			</Col>
			<h5> Full Name: <b> {profileData.firstName} {profileData.lastName} </b> </h5>
			<h5> Email: <b> {profileData.email} </b> </h5>
			<h5> Mobile Number: <b> {profileData.mobileNo} </b> </h5>
			<Button variant="warning" onClick={() => seteditModalShow(true)}>
				<FontAwesomeIcon icon={faEdit} /> Edit Profile</Button>
			<EditProfile
			        show={editModalShow}
			        onHide={() => seteditModalShow(false)}
			        profileData={profileData}
			/>
			</Col>
			</Row>
		</Container>
		</React.Fragment>
	)
}
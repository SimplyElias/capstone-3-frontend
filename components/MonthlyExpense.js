import React, { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import { Button, Alert } from 'react-bootstrap'
import { useRouter } from 'next/router'

export default function MonthlyExpense({expenseData}) {

	const router = useRouter()
	const [show, setShow] = useState(true);
	const handleClick = (e) => {
    e.preventDefault()
    router.reload("/records")
 	}
	// States to store the info in the bar chart
	const [months, setMonths] = useState([]);
	const [monthlyExpenses, setMonthlyExpenses] = useState([]);

	useEffect(() => {

		if(expenseData.length > 0) {

			let tempMonths = []

			expenseData.forEach(element =>	{
				if(!tempMonths.find(month => month === moment(element.recordedOn).format('MMMM') )) {
					tempMonths.push(moment(element.recordedOn).format('MMMM'))
				}
			})
			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

				// Will sort the tempMonth array referncing array
				// 'a' = "January"
				// monthsRef.indexOf("January") = 0
			tempMonths.sort((a, b) => {

				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1) {

					return monthsRef.indexOf(a) - monthsRef.indexOf(b);

				}
			})

			console.log(tempMonths)
			setMonths(tempMonths)
		}

	}, [expenseData])

	useEffect(() => {

		setMonthlyExpenses(months.map(month => {
			let sales = 0;

			expenseData.forEach(element => {
				if(month === moment(element.recordedOn).format('MMMM')) {
					sales = sales + parseInt(element.recordAmount);
				}
			})

			return sales;

		}))
	}, [months])

	console.log(monthlyExpenses)

	const data = {
		labels: months,
		datasets: [
		{
			label: 'Monthly Expenses for 2021',
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255, 99, 132, 1)',
			borderWidth:  1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyExpenses
		}]
	}

	return(
		<React.Fragment>
		{(expenseData.length > 0) ?
			<Bar data={data} />
		:
			<Alert show={show} variant="info">
		        <Alert.Heading>No Records!</Alert.Heading>
		        <p>
		          You currently have no expense records.
		        </p>
		        <hr />
		        <div className="d-flex justify-content-end">
		          <Button onClick={handleClick} variant="outline-info">
		            Go to Records
		          </Button>
		        </div>
		    </Alert>
		
		}
		</React.Fragment>
	)
}
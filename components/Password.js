import React, { useState, UseEffect } from 'react'
import Router from 'next/router'
import { useRouter } from 'next/router'
import Head from "next/head"
import AppHelper from '../app-helper'
import { Container, Row, Col, Card, Image, Button, Form } from 'react-bootstrap'
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faEdit, faTimes } from "@fortawesome/free-solid-svg-icons"; // import the icons you need


export default function ChangePassword() {
	const router = useRouter()

	const [passModalShow, setpassModalShow] = React.useState(false);
	function ChangePass (props) {
	
	const [currentPassword, setcurrentPassword] = useState('')
	const [newPassword, setnewPassword] = useState('')
	const [confirmPassword, setconfirmPassword] = useState('')

	function ChangePassword(e) {
		e.preventDefault();
		const options = { 
			method: "PUT",
			headers: 
			{
			 'Content-Type': 'application/json',
			 Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify(
			{
				password: currentPassword,
				newPassword: newPassword,
				confirmPassword: confirmPassword
			})
		}
		fetch(`${AppHelper.API_URL}/users/change-password`, options) // From the backend.
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			if(data === true) {
				Swal.fire({
					icon: 'success',
					title: `You have updated your password!`,
				}).then(() => {
					router.reload('/profile')
				})
				setpassModalShow(false)
			}
			else if(data.error === "incorrect-password") {
				Swal.fire({
					icon: 'error',
					title: `Your current password is incorrect!`,
				})
			}
			else if(data.error === "incorrect-new-password") {
				Swal.fire({
					icon: 'error',
					title: `Your new passwords don't match!`,
				})
			}
		})
	}
	
  return (

  	<React.Fragment>
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
    <Form onSubmit={(e) => ChangePassword(e)}>
    
    <Modal.Header closeButton>
    	<Modal.Title id="contained-modal-title-vcenter">
        	Edit Profile
        </Modal.Title>
    </Modal.Header>
    <Modal.Body>
    		<Col xs md="6">
			    <Form.Group controlId="currentPassword">
				    <Form.Label>Your current Password: </Form.Label>
				    <Form.Control
				        type="password"
						placeholder="Your Current Password"
				        onChange={(e) => setcurrentPassword(e.target.value)}
				    />
				</Form.Group>
			</Col>
			<br />
        	<Col xs md="6">
			    <Form.Group controlId="newPassword">
				    <Form.Label>New Password: </Form.Label>
				    <Form.Control
				        type="password"
				        placeholder="Your New Password"
				        onChange={(e) => setnewPassword(e.target.value)}
				    />
				</Form.Group>
			</Col>
			<Col xs md="6">
				<Form.Group controlId="confirmPassword">
				    <Form.Label>Confirm New Password:</Form.Label>
				    <Form.Control
				        type="password"
				        placeholder="Your New Password"
				        onChange={(e) => setconfirmPassword(e.target.value)}
				    />
				</Form.Group>
	   		</Col>
    </Modal.Body>
    <Modal.Footer>
    	<Button variant="primary" type="submit" id="submitBtn">
    	<FontAwesomeIcon icon={faEdit} /> Change Password</Button>
      	<Button variant="warning" onClick={props.onHide}>
      	<FontAwesomeIcon icon={faTimes} /> Cancel</Button>
    </Modal.Footer>
    </Form>
    </Modal>
    </React.Fragment>
  );
}



	return (
		<React.Fragment>
		<Container>
			<Row>
			<Col>
			<Col xs={6} md={4}>
			</Col>
				<h4> Change Password </h4>
				<p> You can change your password by pressing this button! </p>
				<Button variant="info" onClick={() => setpassModalShow(true)}>
				<FontAwesomeIcon icon={faEdit} /> Change Password</Button>
				<ChangePass
				        show={passModalShow}
				        onHide={() => setpassModalShow(false)}
				/>
			</Col>
			</Row>
		</Container>
		</React.Fragment>
	)
}